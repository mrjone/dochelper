#ifndef AUTHUSERDIALOG_H
#define AUTHUSERDIALOG_H

//(*Headers(authUserDialog)
#include <wx/button.h>
#include <wx/checkbox.h>
#include <wx/combobox.h>
#include <wx/dialog.h>
#include <wx/gauge.h>
#include <wx/hyperlink.h>
#include <wx/textctrl.h>
//*)

class authUserDialog: public wxDialog
{
	public:

		authUserDialog(wxWindow* parent,wxWindowID id=wxID_ANY,const wxPoint& pos=wxDefaultPosition,const wxSize& size=wxDefaultSize);
		virtual ~authUserDialog();

		//(*Declarations(authUserDialog)
		wxButton* btn_Login;
		wxCheckBox* chb_userRemember;
		wxComboBox* cb_userLogin;
		wxGauge* gau_Progress;
		wxHyperlinkCtrl* HyperlinkCtrl1;
		wxTextCtrl* txt_userPassword;
		//*)

	protected:

		//(*Identifiers(authUserDialog)
		static const long ID_GAUGE1;
		static const long ID_COMBOBOX1;
		static const long ID_TEXTCTRL1;
		static const long ID_CHECKBOX1;
		static const long ID_BUTTON1;
		static const long ID_HYPERLINKCTRL1;
		//*)

	private:

		//(*Handlers(authUserDialog)
		//*)

		DECLARE_EVENT_TABLE()
};

#endif
