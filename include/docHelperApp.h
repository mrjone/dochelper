/***************************************************************
 * Name:      docHelperApp.h
 * Purpose:   Defines Application Class
 * Author:    SKB ()
 * Created:   2021-05-22
 * Copyright: SKB ()
 * License:
 **************************************************************/

#ifndef DOCHELPERAPP_H
#define DOCHELPERAPP_H

#include <wx/app.h>

class docHelperApp : public wxApp
{
    public:
        virtual bool OnInit();
};

#endif // DOCHELPERAPP_H
