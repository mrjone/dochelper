#ifndef SQL_CONNETCOR_H
#define SQL_CONNETCOR_H

#include <mysql.h>
#include <string.h>

#define ERRQ_OK             0U
#define ERRQ_REINIT         1U
#define ERRQ_NOT_INIT       2U
#define ERRQ_CONNECTION     3U
#define ERRQ_DISCONNECT     4U
#define ERRQ_QUERY_DATA     5U
#define ERRQ_QUERY_SEND     6U

#define SQL_DEFAULT_PORT        3306U
#define SQL_DEFAULT_FLAGS       0L
#define SQL_DEFAULT_UNIXSOCK    nullptr

#ifndef MAX_QUERY_LEN
    #define MAX_QUERY_LEN       400U
#endif // MAX_QUERY_LEN


class SQL_CONNETCOR
{
    public:
        SQL_CONNETCOR();
        ~SQL_CONNETCOR();

        unsigned int Init();
        unsigned int Connect(char* host,
                             char* user,
                             char* password,
                             char* database,
                             unsigned int port = SQL_DEFAULT_PORT,
                             char* unix_socket = SQL_DEFAULT_UNIXSOCK,
                             unsigned long flags = SQL_DEFAULT_FLAGS);
        unsigned int Disconnect();

        unsigned int Query(const char* db_query);
        unsigned int Next();

        char* Value(unsigned int num_column);
    private:
        MYSQL init;
        MYSQL* sqlConnection;
        MYSQL_RES* sqlResult;
        MYSQL_ROW sqlRow;
};

#endif // SQL_CONNETCOR_H
