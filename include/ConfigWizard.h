#ifndef CONFIGWIZARD_H
#define CONFIGWIZARD_H


class ConfigWizard
{
    public:
        ConfigWizard();
        ~ConfigWizard();



        char* GetConfigPath();
        void SetConfigPath(char* val);

    private:
        char* ConfigPath;

};

#endif // CONFIGWIZARD_H
