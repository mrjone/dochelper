/***************************************************************
 * Name:      docHelperMain.cpp
 * Purpose:   Code for Application Frame
 * Author:    SKB ()
 * Created:   2021-05-22
 * Copyright: SKB ()
 * License:
 **************************************************************/

#include "docHelperMain.h"
#include <wx/msgdlg.h>

//(*InternalHeaders(docHelperFrame)
#include <wx/intl.h>
#include <wx/settings.h>
#include <wx/string.h>
//*)

//helper functions
enum wxbuildinfoformat {
    short_f, long_f };

wxString wxbuildinfo(wxbuildinfoformat format)
{
    wxString wxbuild(wxVERSION_STRING);

    if (format == long_f )
    {
#if defined(__WXMSW__)
        wxbuild << _T("-Windows");
#elif defined(__UNIX__)
        wxbuild << _T("-Linux");
#endif

#if wxUSE_UNICODE
        wxbuild << _T("-Unicode build");
#else
        wxbuild << _T("-ANSI build");
#endif // wxUSE_UNICODE
    }

    return wxbuild;
}

//(*IdInit(docHelperFrame)
const long docHelperFrame::ID_CHOICE1 = wxNewId();
const long docHelperFrame::ID_CHOICE2 = wxNewId();
const long docHelperFrame::ID_GRID1 = wxNewId();
const long docHelperFrame::ID_CALENDARCTRL1 = wxNewId();
const long docHelperFrame::ID_GRID2 = wxNewId();
const long docHelperFrame::idMenuQuit = wxNewId();
const long docHelperFrame::idMenuAbout = wxNewId();
const long docHelperFrame::ID_STATUSBAR1 = wxNewId();
//*)

BEGIN_EVENT_TABLE(docHelperFrame,wxFrame)
    //(*EventTable(docHelperFrame)
    //*)
END_EVENT_TABLE()

docHelperFrame::docHelperFrame(wxWindow* parent,wxWindowID id)
{
    //(*Initialize(docHelperFrame)
    wxFlexGridSizer* fgs_CtrlTop;
    wxFlexGridSizer* fgs_Grids;
    wxFlexGridSizer* fgs_Main;
    wxFlexGridSizer* fgs_TableNotes;
    wxMenu* Menu1;
    wxMenu* Menu2;
    wxMenuBar* MenuBar1;
    wxMenuItem* MenuItem1;
    wxMenuItem* MenuItem2;

    Create(parent, id, _("DocHelper"), wxDefaultPosition, wxDefaultSize, wxDEFAULT_FRAME_STYLE, _T("id"));
    SetClientSize(wxSize(1000,480));
    SetMinSize(wxSize(1000,480));
    SetBackgroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_WINDOW));
    fgs_Main = new wxFlexGridSizer(1, 2, 0, 0);
    fgs_CtrlTop = new wxFlexGridSizer(5, 2, 0, 0);
    Choice1 = new wxChoice(this, ID_CHOICE1, wxDefaultPosition, wxDefaultSize, 0, 0, 0, wxDefaultValidator, _T("ID_CHOICE1"));
    fgs_CtrlTop->Add(Choice1, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    Choice2 = new wxChoice(this, ID_CHOICE2, wxDefaultPosition, wxDefaultSize, 0, 0, 0, wxDefaultValidator, _T("ID_CHOICE2"));
    fgs_CtrlTop->Add(Choice2, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    fgs_Main->Add(fgs_CtrlTop, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    fgs_Grids = new wxFlexGridSizer(2, 1, 0, 0);
    gDataTable = new wxGrid(this, ID_GRID1, wxDefaultPosition, wxSize(516,237), wxVSCROLL|wxHSCROLL, _T("ID_GRID1"));
    gDataTable->CreateGrid(10,5);
    gDataTable->EnableEditing(true);
    gDataTable->EnableGridLines(true);
    gDataTable->SetRowLabelSize(1);
    gDataTable->SetDefaultColSize(100, true);
    gDataTable->SetColLabelValue(0, _("���"));
    gDataTable->SetColLabelValue(1, wxEmptyString);
    gDataTable->SetColLabelValue(2, wxEmptyString);
    gDataTable->SetCellValue(0, 0, _("�������� �.�."));
    gDataTable->SetCellValue(0, 1, wxEmptyString);
    gDataTable->SetCellValue(0, 2, wxEmptyString);
    gDataTable->SetCellValue(0, 3, wxEmptyString);
    gDataTable->SetCellValue(0, 4, wxEmptyString);
    gDataTable->SetCellValue(1, 0, _("��������� �.�."));
    gDataTable->SetDefaultCellFont( gDataTable->GetFont() );
    gDataTable->SetDefaultCellTextColour( gDataTable->GetForegroundColour() );
    fgs_Grids->Add(gDataTable, 1, wxALL, 5);
    fgs_TableNotes = new wxFlexGridSizer(1, 2, 0, 0);
    dcal_SelectDay = new wxCalendarCtrl(this, ID_CALENDARCTRL1, wxDefaultDateTime, wxDefaultPosition, wxDefaultSize, 0, _T("ID_CALENDARCTRL1"));
    fgs_TableNotes->Add(dcal_SelectDay, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    Grid2 = new wxGrid(this, ID_GRID2, wxDefaultPosition, wxDefaultSize, 0, _T("ID_GRID2"));
    Grid2->CreateGrid(6,3);
    Grid2->EnableEditing(true);
    Grid2->EnableGridLines(true);
    Grid2->SetDefaultCellFont( Grid2->GetFont() );
    Grid2->SetDefaultCellTextColour( Grid2->GetForegroundColour() );
    fgs_TableNotes->Add(Grid2, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
    fgs_Grids->Add(fgs_TableNotes, 1, wxALL|wxEXPAND, 5);
    fgs_Main->Add(fgs_Grids, 1, wxALL|wxEXPAND, 5);
    SetSizer(fgs_Main);
    MenuBar1 = new wxMenuBar();
    Menu1 = new wxMenu();
    MenuItem1 = new wxMenuItem(Menu1, idMenuQuit, _("Quit\tAlt-F4"), _("Quit the application"), wxITEM_NORMAL);
    Menu1->Append(MenuItem1);
    MenuBar1->Append(Menu1, _("&File"));
    Menu2 = new wxMenu();
    MenuItem2 = new wxMenuItem(Menu2, idMenuAbout, _("About\tF1"), _("Show info about this application"), wxITEM_NORMAL);
    Menu2->Append(MenuItem2);
    MenuBar1->Append(Menu2, _("Help"));
    SetMenuBar(MenuBar1);
    StatusBar1 = new wxStatusBar(this, ID_STATUSBAR1, 0, _T("ID_STATUSBAR1"));
    int __wxStatusBarWidths_1[1] = { -1 };
    int __wxStatusBarStyles_1[1] = { wxSB_NORMAL };
    StatusBar1->SetFieldsCount(1,__wxStatusBarWidths_1);
    StatusBar1->SetStatusStyles(1,__wxStatusBarStyles_1);
    SetStatusBar(StatusBar1);
    SetSizer(fgs_Main);
    Layout();

    Connect(idMenuQuit,wxEVT_COMMAND_MENU_SELECTED,(wxObjectEventFunction)&docHelperFrame::OnQuit);
    Connect(idMenuAbout,wxEVT_COMMAND_MENU_SELECTED,(wxObjectEventFunction)&docHelperFrame::OnAbout);
    //*)

    authDialog = new authUserDialog(this);
    authDialog->ShowModal();
}

docHelperFrame::~docHelperFrame()
{
    //(*Destroy(docHelperFrame)
    //*)
}

void docHelperFrame::OnQuit(wxCommandEvent& event)
{
    Close();
}

void docHelperFrame::OnAbout(wxCommandEvent& event)
{
    wxString msg = wxbuildinfo(long_f);
    wxMessageBox(msg, _("Welcome to..."));
}
