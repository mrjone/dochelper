#include "authUserDialog.h"

//(*InternalHeaders(authUserDialog)
#include <wx/intl.h>
#include <wx/string.h>
//*)

//(*IdInit(authUserDialog)
const long authUserDialog::ID_GAUGE1 = wxNewId();
const long authUserDialog::ID_COMBOBOX1 = wxNewId();
const long authUserDialog::ID_TEXTCTRL1 = wxNewId();
const long authUserDialog::ID_CHECKBOX1 = wxNewId();
const long authUserDialog::ID_BUTTON1 = wxNewId();
const long authUserDialog::ID_HYPERLINKCTRL1 = wxNewId();
//*)

BEGIN_EVENT_TABLE(authUserDialog,wxDialog)
	//(*EventTable(authUserDialog)
	//*)
END_EVENT_TABLE()

authUserDialog::authUserDialog(wxWindow* parent,wxWindowID id,const wxPoint& pos,const wxSize& size)
{
	//(*Initialize(authUserDialog)
	Create(parent, id, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxDEFAULT_DIALOG_STYLE, _T("id"));
	SetClientSize(wxSize(278,243));
	Move(wxDefaultPosition);
	gau_Progress = new wxGauge(this, ID_GAUGE1, 100, wxPoint(32,208), wxSize(208,15), 0, wxDefaultValidator, _T("ID_GAUGE1"));
	cb_userLogin = new wxComboBox(this, ID_COMBOBOX1, wxEmptyString, wxPoint(32,40), wxSize(208,23), 0, 0, 0, wxDefaultValidator, _T("ID_COMBOBOX1"));
	txt_userPassword = new wxTextCtrl(this, ID_TEXTCTRL1, wxEmptyString, wxPoint(32,96), wxSize(208,23), wxTE_PASSWORD, wxDefaultValidator, _T("ID_TEXTCTRL1"));
	chb_userRemember = new wxCheckBox(this, ID_CHECKBOX1, _("��������� �����\?"), wxPoint(32,128), wxDefaultSize, 0, wxDefaultValidator, _T("ID_CHECKBOX1"));
	chb_userRemember->SetValue(false);
	btn_Login = new wxButton(this, ID_BUTTON1, _("�����"), wxPoint(168,160), wxDefaultSize, 0, wxDefaultValidator, _T("ID_BUTTON1"));
	HyperlinkCtrl1 = new wxHyperlinkCtrl(this, ID_HYPERLINKCTRL1, _("������ ������\?"), _("https://ya.ru"), wxPoint(32,168), wxDefaultSize, wxHL_CONTEXTMENU|wxHL_ALIGN_CENTRE, _T("ID_HYPERLINKCTRL1"));
	//*)
}

authUserDialog::~authUserDialog()
{
	//(*Destroy(authUserDialog)
	//*)
}

